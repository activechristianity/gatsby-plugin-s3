"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
var chalk_1 = __importDefault(require("chalk"));
var node_fetch_1 = __importDefault(require("node-fetch"));
var constants_1 = require("./constants");
var path_1 = __importDefault(require("path"));
var glob_1 = __importDefault(require("glob"));
var mime_1 = __importDefault(require("mime"));
var TESTING_ENDPOINT = process.env.TESTING_ENDPOINT;
if (!TESTING_ENDPOINT) {
    throw new Error('TESTING_ENDPOINT env var must be set!');
}
console.debug(chalk_1.default(templateObject_1 || (templateObject_1 = __makeTemplateObject(["{blue.bold INFO} using {bold ", "} as endpoint to test against."], ["{blue.bold INFO} using {bold ", "} as endpoint to test against."])), TESTING_ENDPOINT));
var PUBLIC_DIR = path_1.default.join(__dirname, '..', 'examples', 'with-redirects', 'public');
describe('applies caching and content type headers', function () {
    var e_1, _a;
    var _loop_1 = function (pattern) {
        var params = constants_1.CACHING_PARAMS[pattern];
        if (!('CacheControl' in params)) {
            return "continue";
        }
        it("to " + pattern + " files", function () { return __awaiter(_this, void 0, void 0, function () {
            var files, file, response, contentType;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        files = glob_1.default.sync(pattern, { cwd: PUBLIC_DIR, nodir: true });
                        file = files[0];
                        return [4 /*yield*/, node_fetch_1.default(TESTING_ENDPOINT + "/" + file)];
                    case 1:
                        response = _a.sent();
                        contentType = mime_1.default.getType(file) || 'application/octet-stream';
                        expect(response.ok).toBe(true);
                        expect(response.headers.get('cache-control')).toBe(params.CacheControl);
                        expect(response.headers.get('content-type')).toBe(contentType);
                        return [2 /*return*/];
                }
            });
        }); });
    };
    try {
        for (var _b = __values(Object.keys(constants_1.CACHING_PARAMS)), _c = _b.next(); !_c.done; _c = _b.next()) {
            var pattern = _c.value;
            _loop_1(pattern);
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
        }
        finally { if (e_1) throw e_1.error; }
    }
});
describe('redirects', function () {
    test('from the index root', function () { return __awaiter(_this, void 0, void 0, function () {
        var response, followedRedirect;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, node_fetch_1.default(TESTING_ENDPOINT, { redirect: 'manual' })];
                case 1:
                    response = _a.sent();
                    expect(response.status).toBeGreaterThanOrEqual(301);
                    expect(response.status).toBeLessThanOrEqual(302);
                    expect(response.headers.has('location')).toBe(true);
                    return [4 /*yield*/, node_fetch_1.default(response.headers.get('location'))];
                case 2:
                    followedRedirect = _a.sent();
                    expect(followedRedirect.status).toBe(200);
                    return [2 /*return*/];
            }
        });
    }); });
    test('temporarily', function () { return __awaiter(_this, void 0, void 0, function () {
        var response, followedRedirect;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, node_fetch_1.default(TESTING_ENDPOINT + '/hello-there', { redirect: 'manual' })];
                case 1:
                    response = _a.sent();
                    expect(response.headers.get('location')).toBe(TESTING_ENDPOINT + '/client-only');
                    expect(response.status).toBe(302);
                    return [4 /*yield*/, node_fetch_1.default(response.headers.get('location'))];
                case 2:
                    followedRedirect = _a.sent();
                    expect(followedRedirect.status).toBe(200);
                    return [2 /*return*/];
            }
        });
    }); });
    test('permanently with a destination that is prefixed with itself', function () { return __awaiter(_this, void 0, void 0, function () {
        var response, followedRedirect;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, node_fetch_1.default(TESTING_ENDPOINT + '/blog', { redirect: 'manual' })];
                case 1:
                    response = _a.sent();
                    expect(response.status).toBe(301);
                    expect(response.headers.get('location')).toBe(TESTING_ENDPOINT + '/blog/1');
                    return [4 /*yield*/, node_fetch_1.default(response.headers.get('location'))];
                case 2:
                    followedRedirect = _a.sent();
                    expect(followedRedirect.status).toBe(200);
                    return [2 /*return*/];
            }
        });
    }); });
    test('client only routes', function () { return __awaiter(_this, void 0, void 0, function () {
        var response, followedRedirect;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, node_fetch_1.default(TESTING_ENDPOINT + '/client-only/test', { redirect: 'manual' })];
                case 1:
                    response = _a.sent();
                    expect(response.status).toBe(302);
                    expect(response.headers.get('location')).toBe(TESTING_ENDPOINT + '/client-only');
                    return [4 /*yield*/, node_fetch_1.default(response.headers.get('location'))];
                case 2:
                    followedRedirect = _a.sent();
                    expect(followedRedirect.status).toBe(200);
                    return [2 /*return*/];
            }
        });
    }); });
    test('special characters using WebsiteRedirectLocation', function () { return __awaiter(_this, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, node_fetch_1.default(TESTING_ENDPOINT + '/asdf123.-~_!%24%26\'()*%2B%2C%3B%3D%3A%40%25', { redirect: 'manual' })];
                case 1:
                    response = _a.sent();
                    expect(response.status).toBe(301);
                    expect(response.headers.get('location')).toBe(TESTING_ENDPOINT + '/special-characters');
                    return [2 /*return*/];
            }
        });
    }); });
    test('trailing slash using WebsiteRedirectLocation', function () { return __awaiter(_this, void 0, void 0, function () {
        var response;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, node_fetch_1.default(TESTING_ENDPOINT + '/trailing-slash/', { redirect: 'manual' })];
                case 1:
                    response = _a.sent();
                    expect(response.status).toBe(301);
                    expect(response.headers.get('location')).toBe(TESTING_ENDPOINT + '/trailing-slash/1');
                    return [2 /*return*/];
            }
        });
    }); });
});
var templateObject_1;
//# sourceMappingURL=bin.test.js.map