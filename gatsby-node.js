"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("./constants");
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var util_1 = require("./util");
// converts gatsby redirects + rewrites to S3 routing rules
// https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-s3-websiteconfiguration-routingrules.html
var getRules = function (pluginOptions, routes) { return (routes.map(function (route) { return ({
    Condition: {
        KeyPrefixEquals: util_1.withoutLeadingSlash(route.fromPath),
        HttpErrorCodeReturnedEquals: '404',
    },
    Redirect: {
        ReplaceKeyWith: util_1.withoutTrailingSlash(util_1.withoutLeadingSlash(route.toPath)),
        HttpRedirectCode: route.isPermanent ? '301' : '302',
        Protocol: pluginOptions.protocol,
        HostName: pluginOptions.hostname,
    },
}); })); };
var params = {};
exports.onPreBootstrap = function (_a, _b) {
    var reporter = _a.reporter;
    var bucketName = _b.bucketName;
    if (!bucketName) {
        reporter.panic("\n      \"bucketName\" is a required option for gatsby-plugin-s3\n      See docs here - https://github.com/jariz/gatsby-plugin-s3\n      ");
        process.exit(1);
    }
    params = {};
};
exports.createPagesStatefully = function (_a, userPluginOptions) {
    var store = _a.store, createPage = _a.actions.createPage;
    var pluginOptions = __assign({}, constants_1.DEFAULT_OPTIONS, userPluginOptions);
    var _b = store.getState(), redirects = _b.redirects, pages = _b.pages;
    if (pluginOptions.generateIndexPageForRedirect) {
        var indexRedirect = redirects.find(function (redirect) { return redirect.fromPath === '/'; });
        var indexPage = Array.from(pages.values()).find(function (page) { return page.path === '/'; });
        if (indexRedirect) {
            if (!indexPage) {
                // no index page yet, create one so we can add a redirect to it's metadata when uploading
                createPage({
                    path: '/',
                    component: path_1.default.join(__dirname, './fake-index.js'),
                });
            }
            params = __assign({}, params, { 'index.html': {
                    WebsiteRedirectLocation: indexRedirect.toPath,
                } });
        }
    }
};
exports.onPostBuild = function (_a, userPluginOptions) {
    var store = _a.store;
    var pluginOptions = __assign({}, constants_1.DEFAULT_OPTIONS, userPluginOptions);
    var _b = store.getState(), redirects = _b.redirects, pages = _b.pages, program = _b.program;
    if (!pluginOptions.hostname != !pluginOptions.protocol) { // If one of these is provided but not the other
        throw new Error("Please either provide both 'hostname' and 'protocol', or neither of them.");
    }
    var rewrites = [];
    if (pluginOptions.generateMatchPathRewrites) {
        rewrites = Array.from(pages.values())
            .filter(function (page) { return !!page.matchPath && page.matchPath !== page.path; })
            .map(function (page) { return ({
            // sort of (w)hack. https://i.giphy.com/media/iN5qfn8S2qVgI/giphy.webp
            // the syntax that gatsby invented here does not work with routing rules.
            // routing rules syntax is `/app/` not `/app/*` (it's basically prefix by default)
            fromPath: page.matchPath.endsWith('*')
                ? page.matchPath.substring(0, page.matchPath.length - 1)
                : page.matchPath,
            toPath: page.path,
        }); });
    }
    if (pluginOptions.mergeCachingParams) {
        params = __assign({}, params, constants_1.CACHING_PARAMS);
    }
    params = __assign({}, params, pluginOptions.params);
    var routingRules = [];
    var slsRoutingRules = [];
    var temporaryRedirects = redirects.filter(function (redirect) { return redirect.fromPath !== '/'; })
        .filter(function (redirect) { return !redirect.isPermanent; });
    var permanentRedirects = redirects.filter(function (redirect) { return redirect.fromPath !== '/'; })
        .filter(function (redirect) { return redirect.isPermanent; });
    if (pluginOptions.generateRoutingRules) {
        routingRules = __spread(getRules(pluginOptions, temporaryRedirects), getRules(pluginOptions, rewrites));
        if (!pluginOptions.generateRedirectObjectsForPermanentRedirects) {
            routingRules.push.apply(routingRules, __spread(getRules(pluginOptions, permanentRedirects)));
        }
        if (routingRules.length > 50) {
            throw new Error(routingRules.length + " routing rules provided, the number of routing rules \nin a website configuration is limited to 50.\nTry setting the 'generateRedirectObjectsForPermanentRedirects' configuration option.");
        }
        slsRoutingRules = routingRules.map(function (_a) {
            var Redirect = _a.Redirect, Condition = _a.Condition;
            return ({
                RoutingRuleCondition: Condition,
                RedirectRule: Redirect,
            });
        });
    }
    fs_1.default.writeFileSync(path_1.default.join(program.directory, './.cache/s3.routingRules.json'), JSON.stringify(routingRules));
    fs_1.default.writeFileSync(path_1.default.join(program.directory, './.cache/s3.sls.routingRules.json'), JSON.stringify(slsRoutingRules));
    if (pluginOptions.generateRedirectObjectsForPermanentRedirects) {
        fs_1.default.writeFileSync(path_1.default.join(program.directory, './.cache/s3.redirectObjects.json'), JSON.stringify(permanentRedirects));
    }
    fs_1.default.writeFileSync(path_1.default.join(program.directory, './.cache/s3.params.json'), JSON.stringify(params));
    fs_1.default.writeFileSync(path_1.default.join(program.directory, './.cache/s3.config.json'), JSON.stringify(pluginOptions));
};
//# sourceMappingURL=gatsby-node.js.map