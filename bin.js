#!/usr/bin/env node
"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function (o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
require("@babel/polyfill");
require("fs-posix");
var s3_1 = __importDefault(require("aws-sdk/clients/s3"));
var yargs_1 = __importDefault(require("yargs"));
var constants_1 = require("./constants");
var fs_extra_1 = require("fs-extra");
var klaw_1 = __importDefault(require("klaw"));
var pretty_error_1 = __importDefault(require("pretty-error"));
var stream_to_promise_1 = __importDefault(require("stream-to-promise"));
var ora_1 = __importDefault(require("ora"));
var chalk_1 = __importDefault(require("chalk"));
var path_1 = require("path");
var url_1 = require("url");
var fs_1 = __importDefault(require("fs"));
var util_1 = __importDefault(require("util"));
var minimatch_1 = __importDefault(require("minimatch"));
var mime_1 = __importDefault(require("mime"));
var inquirer_1 = __importDefault(require("inquirer"));
var aws_sdk_1 = require("aws-sdk");
var crypto_1 = require("crypto");
var is_ci_1 = __importDefault(require("is-ci"));
var util_2 = require("./util");
var async_1 = require("async");
var json_stringify_safe_1 = __importDefault(require("json-stringify-safe"));
var shouldValidatePaths = [];
var cli = yargs_1.default();
var pe = new pretty_error_1.default();
var OBJECTS_TO_REMOVE_PER_REQUEST = 1000;
var promisifiedParallelLimit = util_1.default.promisify(async_1.parallelLimit);
var guessRegion = function (s3, constraint) { return (constraint || s3.config.region || aws_sdk_1.config.region); };
var getBucketInfo = function (config, s3) { return __awaiter(_this, void 0, void 0, function () {
    var $response, detectedRegion, ex_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, s3.getBucketLocation({ Bucket: config.bucketName }).promise()];
            case 1:
                $response = (_a.sent()).$response;
                detectedRegion = guessRegion(s3, ($response.data && $response.data.LocationConstraint));
                return [2 /*return*/, {
                        exists: true,
                        region: detectedRegion,
                    }];
            case 2:
                ex_1 = _a.sent();
                if (ex_1.code === 'NoSuchBucket') {
                    return [2 /*return*/, {
                            exists: false,
                            region: guessRegion(s3),
                        }];
                }
                else {
                    throw ex_1;
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); };
var getParams = function (path, params) {
    var e_1, _a;
    var returned = {};
    try {
        for (var _b = __values(Object.keys(params)), _c = _b.next(); !_c.done; _c = _b.next()) {
            var key = _c.value;
            if (minimatch_1.default(path, key)) {
                returned = __assign({}, returned, params[key]);
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
        }
        finally { if (e_1) throw e_1.error; }
    }
    return returned;
};
var listAllObjects = function (s3, bucketName) { return __awaiter(_this, void 0, void 0, function () {
    var list, token, response;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                list = [];
                _a.label = 1;
            case 1: return [4 /*yield*/, s3.listObjectsV2({
                    Bucket: bucketName,
                    ContinuationToken: token,
                }).promise()];
            case 2:
                response = _a.sent();
                if (response.Contents) {
                    list.push.apply(list, __spread(response.Contents));
                }
                token = response.NextContinuationToken;
                _a.label = 3;
            case 3:
                if (token) return [3 /*break*/, 1];
                _a.label = 4;
            case 4: return [2 /*return*/, list];
        }
    });
}); };
var createSafeS3Key = function (key) {
    if (path_1.sep === '\\') {
        return key.replace(/\\/g, '/');
    }
    return key;
};
var deploy = function (_a) {
    var yes = _a.yes, bucket = _a.bucket;
    return __awaiter(_this, void 0, void 0, function () {
        var spinner, uploadQueue, config_1, params_1, routingRules, redirectObjects, _b, s3_2, _c, exists, region, confirm_1, createParams, websiteConfig, objects_1, publicDir_1, stream, isKeyInUse_1, base_1, objectsToRemove, i, objectsToRemoveInThisRequest, invalidatePathsData, s3WebsiteDomain, ex_2;
        var _this = this;
        return __generator(this, function (_d) {
            switch (_d.label) {
                case 0:
                    spinner = ora_1.default({ text: 'Retrieving bucket info...', color: 'magenta' }).start();
                    uploadQueue = [];
                    _d.label = 1;
                case 1:
                    _d.trys.push([1, 22, , 23]);
                    return [4 /*yield*/, fs_extra_1.readJson(constants_1.CACHE_FILES.config)];
                case 2:
                    config_1 = _d.sent();
                    return [4 /*yield*/, fs_extra_1.readJson(constants_1.CACHE_FILES.params)];
                case 3:
                    params_1 = _d.sent();
                    return [4 /*yield*/, fs_extra_1.readJson(constants_1.CACHE_FILES.routingRules)];
                case 4:
                    routingRules = _d.sent();
                    if (!fs_1.default.existsSync(constants_1.CACHE_FILES.redirectObjects)) return [3 /*break*/, 6];
                    return [4 /*yield*/, fs_extra_1.readJson(constants_1.CACHE_FILES.redirectObjects)];
                case 5:
                    _b = _d.sent();
                    return [3 /*break*/, 7];
                case 6:
                    _b = [];
                    _d.label = 7;
                case 7:
                    redirectObjects = _b;
                    // Override the bucket name if it is set via command line
                    if (bucket) {
                        config_1.bucketName = bucket;
                    }
                    s3_2 = new s3_1.default({
                        region: config_1.region,
                        endpoint: config_1.customAwsEndpointHostname,
                    });
                    return [4 /*yield*/, getBucketInfo(config_1, s3_2)];
                case 8:
                    _c = _d.sent(), exists = _c.exists, region = _c.region;
                    if (is_ci_1.default && !yes) {
                        yes = true;
                    }
                    if (!!yes) return [3 /*break*/, 10];
                    spinner.stop();
                    console.log(chalk_1.default(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n    {underline Please review the following:} ({dim pass -y next time to skip this})\n\n    Deploying to bucket: {cyan.bold ", "}\n    In region: {yellow.bold ", "}\n    Gatsby will: ", "\n"], ["\n    {underline Please review the following:} ({dim pass -y next time to skip this})\n\n    Deploying to bucket: {cyan.bold ", "}\n    In region: {yellow.bold ", "}\n    Gatsby will: ",
                        "\n"])), config_1.bucketName, region || 'UNKNOWN!', !exists
                        ? chalk_1.default(templateObject_1 || (templateObject_1 = __makeTemplateObject(["{bold.greenBright CREATE}"], ["{bold.greenBright CREATE}"]))) : chalk_1.default(templateObject_2 || (templateObject_2 = __makeTemplateObject(["{bold.blueBright UPDATE} {dim (any existing website configuration will be overwritten!)}"], ["{bold.blueBright UPDATE} {dim (any existing website configuration will be overwritten!)}"])))));
                    return [4 /*yield*/, inquirer_1.default.prompt([{
                                message: 'OK?',
                                name: 'confirm',
                                type: 'confirm',
                            }])];
                case 9:
                    confirm_1 = (_d.sent()).confirm;
                    if (!confirm_1) {
                        throw new Error('User aborted!');
                    }
                    spinner.start();
                    _d.label = 10;
                case 10:
                    spinner.text = 'Configuring bucket...';
                    spinner.color = 'yellow';
                    if (!!exists) return [3 /*break*/, 12];
                    createParams = {
                        Bucket: config_1.bucketName,
                        ACL: config_1.acl === null ? undefined : (config_1.acl || 'public-read'),
                    };
                    if (config_1.region) {
                        createParams.CreateBucketConfiguration = {
                            LocationConstraint: config_1.region,
                        };
                    }
                    return [4 /*yield*/, s3_2.createBucket(createParams).promise()];
                case 11:
                    _d.sent();
                    _d.label = 12;
                case 12:
                    if (!config_1.enableS3StaticWebsiteHosting) return [3 /*break*/, 14];
                    websiteConfig = {
                        Bucket: config_1.bucketName,
                        WebsiteConfiguration: {
                            IndexDocument: {
                                Suffix: 'index.html',
                            },
                            ErrorDocument: {
                                Key: '404.html',
                            },
                        },
                    };
                    if (routingRules.length) {
                        websiteConfig.WebsiteConfiguration.RoutingRules = routingRules;
                    }
                    return [4 /*yield*/, s3_2.putBucketWebsite(websiteConfig).promise()];
                case 13:
                    _d.sent();
                    _d.label = 14;
                case 14:
                    spinner.text = 'Listing objects...';
                    spinner.color = 'green';
                    return [4 /*yield*/, listAllObjects(s3_2, config_1.bucketName)];
                case 15:
                    objects_1 = _d.sent();
                    spinner.color = 'cyan';
                    spinner.text = 'Syncing...';
                    publicDir_1 = path_1.resolve('./public');
                    stream = klaw_1.default(publicDir_1);
                    isKeyInUse_1 = {};
                    stream.on('data', function (_a) {
                        var path = _a.path, stats = _a.stats;
                        return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_b) {
                                if (!stats.isFile()) {
                                    return [2 /*return*/];
                                }
                                uploadQueue.push(async_1.asyncify(function () { return __awaiter(_this, void 0, void 0, function () {
                                    var key, readStream, hashStream, data, tag, object, upload, ex_3;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                key = createSafeS3Key(path_1.relative(publicDir_1, path));
                                                readStream = fs_1.default.createReadStream(path);
                                                hashStream = readStream.pipe(crypto_1.createHash('md5').setEncoding('hex'));
                                                return [4 /*yield*/, stream_to_promise_1.default(hashStream)];
                                            case 1:
                                                data = _a.sent();
                                                tag = "\"" + data + "\"";
                                                object = objects_1.find(function (currObj) { return currObj.Key === key && currObj.ETag === tag; });
                                                isKeyInUse_1[key] = true;
                                                if (!!object) return [3 /*break*/, 5];
                                                shouldValidatePaths.push("/" + key);
                                                _a.label = 2;
                                            case 2:
                                                _a.trys.push([2, 4, , 5]);
                                                upload = new s3_1.default.ManagedUpload({
                                                    service: s3_2,
                                                    params: __assign({ Bucket: config_1.bucketName, Key: key, Body: fs_1.default.createReadStream(path), ACL: config_1.acl === null ? undefined : (config_1.acl || 'public-read'), ContentType: mime_1.default.getType(path) || 'application/octet-stream' }, getParams(key, params_1)),
                                                });
                                                upload.on('httpUploadProgress', function (evt) {
                                                    spinner.text = chalk_1.default(templateObject_4 || (templateObject_4 = __makeTemplateObject(["Syncing...\n{dim   Uploading {cyan ", "} ", "/", "}"], ["Syncing...\n{dim   Uploading {cyan ", "} ", "/", "}"])), key, evt.loaded.toString(), evt.total.toString());
                                                });
                                                return [4 /*yield*/, upload.promise()];
                                            case 3:
                                                _a.sent();
                                                spinner.text = chalk_1.default(templateObject_5 || (templateObject_5 = __makeTemplateObject(["Syncing...\n{dim   Uploaded {cyan ", "}}"], ["Syncing...\\n{dim   Uploaded {cyan ", "}}"])), key);
                                                return [3 /*break*/, 5];
                                            case 4:
                                                ex_3 = _a.sent();
                                                console.error(ex_3);
                                                process.exit(1);
                                                return [3 /*break*/, 5];
                                            case 5: return [2 /*return*/];
                                        }
                                    });
                                }); }));
                                return [2 /*return*/];
                            });
                        });
                    });
                    base_1 = (config_1.protocol && config_1.hostname) ? config_1.protocol + "://" + config_1.hostname : null;
                    uploadQueue.push.apply(uploadQueue, __spread(redirectObjects.map(function (redirect) {
                        return async_1.asyncify(function () { return __awaiter(_this, void 0, void 0, function () {
                            var fromPath, redirectPath, redirectLocation, key, tag, object, upload, ex_4;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        fromPath = redirect.fromPath, redirectPath = redirect.toPath;
                                        redirectLocation = base_1 ? url_1.resolve(base_1, redirectPath) : redirectPath;
                                        key = util_2.withoutLeadingSlash(fromPath);
                                        if (/\/$/.test(key)) {
                                            key = path_1.join(key, 'index.html');
                                        }
                                        key = createSafeS3Key(key);
                                        tag = "\"" + crypto_1.createHash('md5').update(redirectLocation).digest('hex') + "\"";
                                        object = objects_1.find(function (currObj) { return currObj.Key === key && currObj.ETag === tag; });
                                        isKeyInUse_1[key] = true;
                                        if (object) {
                                            // object with exact hash already exists, abort.
                                            return [2 /*return*/];
                                        }
                                        _a.label = 1;
                                    case 1:
                                        _a.trys.push([1, 3, , 4]);
                                        shouldValidatePaths.push("/" + key);
                                        upload = new s3_1.default.ManagedUpload({
                                            service: s3_2,
                                            params: __assign({ Bucket: config_1.bucketName, Key: key, Body: redirectLocation, ACL: config_1.acl === null ? undefined : (config_1.acl || 'public-read'), ContentType: 'application/octet-stream', WebsiteRedirectLocation: redirectLocation }, getParams(key, params_1)),
                                        });
                                        return [4 /*yield*/, upload.promise()];
                                    case 2:
                                        _a.sent();
                                        spinner.text = chalk_1.default(templateObject_6 || (templateObject_6 = __makeTemplateObject(["Syncing...\n{dim   Created Redirect {cyan ", "} => {cyan ", "}}\n"], ["Syncing...\n{dim   Created Redirect {cyan ", "} => {cyan ", "}}\\n"])), key, redirectLocation);
                                        return [3 /*break*/, 4];
                                    case 3:
                                        ex_4 = _a.sent();
                                        spinner.fail(chalk_1.default(templateObject_7 || (templateObject_7 = __makeTemplateObject(["Upload failure for object {cyan ", "}"], ["Upload failure for object {cyan ", "}"])), key));
                                        console.error(pe.render(ex_4));
                                        process.exit(1);
                                        return [3 /*break*/, 4];
                                    case 4: return [2 /*return*/];
                                }
                            });
                        }); });
                    })));
                    // tslint:disable-next-line:no-any todo: find out why the typing won't allow this as-is
                    return [4 /*yield*/, stream_to_promise_1.default(stream)];
                case 16:
                    // tslint:disable-next-line:no-any todo: find out why the typing won't allow this as-is
                    _d.sent();
                    return [4 /*yield*/, promisifiedParallelLimit(uploadQueue, 20)];
                case 17:
                    _d.sent();
                    if (!config_1.removeNonexistentObjects) return [3 /*break*/, 21];
                    objectsToRemove = objects_1.map(function (obj) { return ({ Key: obj.Key }); })
                        .filter(function (obj) { return obj.Key && !isKeyInUse_1[obj.Key]; });
                    i = 0;
                    _d.label = 18;
                case 18:
                    if (!(i < objectsToRemove.length)) return [3 /*break*/, 21];
                    objectsToRemoveInThisRequest = objectsToRemove.slice(i, i + OBJECTS_TO_REMOVE_PER_REQUEST);
                    spinner.text =
                        "Removing objects " + (i + 1) + " to " + (i +
                            objectsToRemoveInThisRequest.length) + " of " + objectsToRemove.length;
                    return [4 /*yield*/, s3_2.deleteObjects({
                            Bucket: config_1.bucketName,
                            Delete: {
                                Objects: objectsToRemoveInThisRequest,
                                Quiet: true,
                            },
                        }).promise()];
                case 19:
                    _d.sent();
                    _d.label = 20;
                case 20:
                    i += OBJECTS_TO_REMOVE_PER_REQUEST;
                    return [3 /*break*/, 18];
                case 21:
                    spinner.succeed('Synced.');
                    invalidatePathsData = {
                        Paths: {
                            Quantity: shouldValidatePaths.length,
                            Items: shouldValidatePaths,
                        },
                        CallerReference: "ac-" + Date.now(),
                    };
                    console.log(invalidatePathsData);
                    fs_1.default.writeFileSync('invbatch.json', json_stringify_safe_1.default(invalidatePathsData, null, 2));
                    /*         const rawdata = fs.readFileSync('invbatch.json');
                            const json = JSON.parse(rawdata.toString('utf8'));
                            console.log(json); */
                    if (config_1.enableS3StaticWebsiteHosting) {
                        s3WebsiteDomain = util_2.getS3WebsiteDomainUrl(region || 'us-east-1');
                        console.log(chalk_1.default(templateObject_8 || (templateObject_8 = __makeTemplateObject(["\n            {bold Your website is online at:}\n            {blue.underline http://", ".", "}\n            "], ["\n            {bold Your website is online at:}\n            {blue.underline http://", ".", "}\n            "])), config_1.bucketName, s3WebsiteDomain));
                    }
                    else {
                        console.log(chalk_1.default(templateObject_9 || (templateObject_9 = __makeTemplateObject(["\n            {bold Your website has now been published to:}\n            {blue.underline ", "}\n            "], ["\n            {bold Your website has now been published to:}\n            {blue.underline ", "}\n            "])), config_1.bucketName));
                    }
                    return [3 /*break*/, 23];
                case 22:
                    ex_2 = _d.sent();
                    spinner.fail('Failed.');
                    console.error(pe.render(ex_2));
                    process.exit(1);
                    return [3 /*break*/, 23];
                case 23: return [2 /*return*/];
            }
        });
    });
};
cli
    .command(['deploy', '$0'], 'Deploy bucket. If it doesn\'t exist, it will be created. Otherwise, it will be updated.', function (args) {
    args.option('yes', {
        alias: 'y',
        describe: 'Skip confirmation prompt',
        boolean: true,
    });
    args.option('bucket', {
        alias: 'b',
        describe: 'Bucket name (if you wish to override default bucket name)',
    });
}, deploy)
    .wrap(cli.terminalWidth())
    .demandCommand(1, "Pass --help to see all available commands and options.")
    .strict()
    .showHelpOnFail(true)
    .recommendCommands()
    .parse(process.argv.slice(2));
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9;
/* const parsePath = (path: string, extension: string) => {
    if (path.startsWith('/')) {
        return `${encodeURIComponent(path)}.${extension}`;
    } else {
        console.log(path);
        return `/${encodeURIComponent(path)}.${extension}`;
    }
}; */
//# sourceMappingURL=bin.js.map